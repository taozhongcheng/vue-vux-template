# 移动端vue基于vux框架
* 项目背景：
  * `master`主分支，所有其他分支都应从此分支拉取

## Git
  > [git@https://gitlab.com/taozhongcheng/vue-vux-template.git](https://gitlab.com/taozhongcheng/vue-vux-template.git)

## 项目运行
```shell
npm i

&&

npm run serve

## 发布及部署

1. 打包配置文件详见`vue.config.js`
2. 接口配置文件详见`src/config/config.js`
3. 打包命令
```shell
npm run build
```
>  服务器配置路径映射应为`dist/index.html`

## 协同开发必看
1. **editorconfig**
为保证代码缩进、换行等统一，务必请安转`editorconfig`插件！，
详情配置详见`.editorconfig`文件

2. 代码规范
推荐代码规范： [前端工程规范](https://github.com/Haonancx/fees)
*老伙计，前端规范，了解一下*

3. Git commit 提交规范
[Git 提交的正确姿势：Commit message 编写指南](https://wiki.jiatuiyun.net/pages/viewpage.action?pageId=8028412)


