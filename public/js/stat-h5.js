var STAT_CONFIG = {
	api_url: 'http://dev-cms.aijiatui.com/api/auth/storage',
	version: '0.0.1',
	prefix: '_stat_',
	cid: '',
	pid: '',
	uid: '',
	auto_check_DOM: false
};

(function getConfig(){
	var scriptEl = document.getElementsByName("STAT");

	if(scriptEl.length === 0){
		var scripts = document.getElementsByTagName("script")
		for(var i = 0; i < scripts.length; i++){
			if(typeof scripts[i].attributes.name !== "undefined" && scripts[i].attributes.name.nodeValue == "MTAH5"){
				var scriptEl = [];
				scriptEl.push(scripts[i]);
				break;
			}
		}
	}

	if(scriptEl.length > 0){
		typeof scriptEl[0].attributes.cid !== "undefined" && (STAT_CONFIG.cid = scriptEl[0].attributes.cid.nodeValue);
  		typeof scriptEl[0].attributes.pid !== "undefined" && (STAT_CONFIG.pid = scriptEl[0].attributes.pid.nodeValue);
  		typeof scriptEl[0].attributes.uid !== "undefined" && (STAT_CONFIG.uid = scriptEl[0].attributes.uid.nodeValue);
	}

	if(typeof _stat_config === "object"){
		for(var key in _stat_config){
			_stat_config.hasOwnProperty(key) && (STAT_CONFIG[key] = _stat_config[key]);
		}
	}
})();

function getStorage(key){
	if(window.localStorage){
		return localStorage.getItem(STAT_CONFIG.prefix + key) || sessionStorage.getItem(STAT_CONFIG.prefix + key)
	} else {
		var values = document.cookie.match(new RegExp("(?:^|;\\s)" + (STAT_CONFIG.prefix + key) +"=(.*?)(?:;\\s|$)"));
		return values ? values[1] : '';
	}
}

function setStorage(key, value, expires){
	if(window.localStorage){
		try{
			expires ? localStorage.setItem(STAT_CONFIG.prefix + key, value) : sessionStorage.setItem(STAT_CONFIG.prefix + key, value)
		}catch(e){}
	} else{
		var host = window.location.host;
		var h = {"com.cn": 1, "js.cn": 1, "net.cn": 1, "gov.cn": 1, "com.hk": 1, "co.nz": 1};
		var path = host.split(".");
		path.length < 2 && (host = (h[path.slice(-2).join(".")] ? path.slice(-3) : path.slice(-2)).join("."));
		document.cookie = STAT_CONFIG.prefix + key + "=" + value + ";path=/;domain=" + host + (expires ? ";expires=" + expires : "")
	}
}

function analysisUrl(url){
	var param = {};
	var location = window.location;
	var host = location.host;
	var pathname = location.pathname;
	var search = location.search.substr(1);
	var hash = location.hash;

	if(url){
		var results = url.match(/\w+:\/\/((?:[\w-]+\.)+\w+)(?::\d+)?(\/[^\?\\"'\|:<>]*)?(?:\?([^'"\\<>#]*))?(?:#(\w+))?/i) || [];
		host = results[1];
		pathname = results[2];
		search = results[3];
		hash = results[4];
	}

	hash !== void 0 && (hash = hash.replace(/"|'|<|>/ig, "M"));

	search = function(){
		if(typeof search === "undefined") return search;
		var params = {};
		var group = search.split("&");
		for(var i = 0; i < group.length; i++){
			var index = group[i].indexOf("=");
			if(index != -1){
				var key = group[i].slice(0, index);
				var value = group[i].slice(index + 1);
				params[key] = value;
			}
		}
		return params;
	}();

	return {
		host: host,
		path: pathname,
		search: search,
		hash: hash
	}
}

function getRandom(a){
	for(var d = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], c = 10; 1 < c; c--){
		var b = Math.floor(10 * Math.random());
		var h = d[b];

		d[b] = d[c - 1];
		d[c - 1] = h
	}

	for(c = b = 0; 5 > c; c++){
		b = 10 * b + d[c];
	}

	return(a || "") + (b + "" + +new Date);
}

function getSystemInfo(){
	var data = {
		pixelRatio: window.devicePixelRatio || '',
		screenWidth: null,
		screenHeight: null,
		language: null
	}

	try{
		var nav = window.navigator;
		var screen = window.screen || {
			width: "",
			height: ""
		}

		data.screenWidth = screen.width,
		data.screenHeight = screen.height,
		data.language = (nav.language || nav.userLanguage).toLowerCase()
		data.userAgent = nav.userAgent || '';
	}catch(e){}

	return data;
}

function getBasicInfo(){
	var location = analysisUrl();
	var info = {
		pid: STAT_CONFIG.pid,
		cid: STAT_CONFIG.cid,
		uid: '',
		env: 'h5',
		url: location.path
	};

	// 获取并存储uid
	info.uid = function(){
		var uid = getStorage("uid");
		if(!uid){
			uid = getRandom();
			setStorage("uid", uid, "Mon, 30 Jul 2028 00:00:00 GMT;");
		}
		return uid;
	}();

	return info;
}

function getRef(){
	var ref = analysisUrl(document.referrer);
	var url = analysisUrl();
	return{
		host: ref.host,
		path: ref.path,
		search: ref.search,
		statSrc: url._stat_src
	}
}

function buildData(data){
	var url = data.url;
	var ref = data.ref;

	return {
		uuid: data.uid,
		corpid: data.cid,
		productid: data.pid,
		env: data.env,
		type: data.eventType,
		timestamp: +new Date,
		systemInfo:{
			screenWidth: data.screenWidth,
			screenHeight: data.screenHeight,
			language: data.language,
			pixelRatio: data.pixelRatio,
			userAgent: data.userAgent,

			// brand: '',
			// model:'',
			// networkType: '',
			// latitude: '',
			// longitude: '',
			// version:'',
			// system:'',
			// platform:'',
			// wxVersion: '',
			// wxSDKVersion: ''
		},
		event: {
			// pageId: '',
			pageUrl: url.path,
			eventId: data.event,
			eventExt: data.eventExt,
			urlExt: {
				url: url,
				ref: ref
			}
		},
		userInfo: {
			userId: STAT_CONFIG.uid || ''
		}
	}
}

function sendData(formData){
	// var request = new XMLHttpRequest();
	// request.open('POST', STAT_CONFIG.api_url, true);
	// request.send(JSON.stringify(formData));

	var queryParams = [];
	for(var key in formData){
		if(typeof formData[key] === 'object'){
			queryParams.push(key + '=' + encodeURIComponent(JSON.stringify(formData[key])))
			continue;
		}
		queryParams.push(key + '=' + encodeURIComponent(formData[key]));
	}

	var url = STAT_CONFIG.api_url + '?' + queryParams.join('&');
	var img = new Image;
	img.onload = img.onerror = img.onabort = function(){
		img.onload = img.onerror = img.onabort = null;
	}
	img.src = url;
}

var Stat = {};

Stat.sendEvent = function(eventId, params){
	var baseInfo = getBasicInfo();

	if(!baseInfo.pid) return console.log(new Error('统计SDK未添加pid'));

	var systemInfo = getSystemInfo();
	var url = analysisUrl();
	var ref = getRef();

	var extInfo = function(params){
		params = typeof params === "undefined" ? {} : params;
		var obj = {};
		for(var key in params){
			(key !== 'eventType') && params.hasOwnProperty(key) && (obj[key] = params[key]);
		}
		return obj;
	}(params);

	var data = [
		baseInfo,
		systemInfo,
		{
			event: eventId,
			eventType: (params && params.eventType || 'click'),
			url: url,
			ref: ref,
			eventExt: extInfo,
			timestamp: +new Date
		}
	];
	var formData = {};

	for(var j = 0; j < data.length; j++){
		for(var i in data[j]){
			data[j].hasOwnProperty(i) && (formData[i]= (typeof data[j][i] === 'undefined' ? '' : data[j][i]));
		}
	}

	sendData(buildData(formData));
}

Stat.init = function(opt){
	opt && opt.cid && (STAT_CONFIG.cid = opt.cid);
	opt && opt.pid && (STAT_CONFIG.pid = opt.pid);
	opt && opt.uid && (STAT_CONFIG.uid = opt.uid);
	init();
}

function init(){
	var baseInfo = getBasicInfo();

	if(!baseInfo.pid) return console.log(new Error('统计SDK未添加pid'));

	var systemInfo = getSystemInfo();
	var url = analysisUrl();
	var ref = getRef();

	var data = [
		baseInfo,
		systemInfo,
		{
			event: 'load',
			eventType: 'page',
			url: url,
			ref: ref,
			eventExt: {}
		}
	];
	var formData = {};

	for(var j = 0; j < data.length; j++){
		for(var i in data[j]){
			data[j].hasOwnProperty(i) && (formData[i]= (typeof data[j][i] === 'undefined' ? '' : data[j][i]));
		}
	}

	sendData(buildData(formData));
};

window.addEventListener('beforeunload', function(e){
	var baseInfo = getBasicInfo();

	if(!baseInfo.pid) return console.log(new Error('统计SDK未添加pid'));

	var systemInfo = getSystemInfo();
	var url = analysisUrl();
	var ref = getRef();

	var data = [
		baseInfo,
		systemInfo,
		{
			event: 'unload',
			eventType: 'page',
			url: url,
			ref: ref,
			eventExt: {}
		}
	];
	var formData = {};

	for(var j = 0; j < data.length; j++){
		for(var i in data[j]){
			data[j].hasOwnProperty(i) && (formData[i]= (typeof data[j][i] === 'undefined' ? '' : data[j][i]));
		}
	}

	sendData(buildData(formData));
}, false);

STAT_CONFIG.auto_check_DOM && function(){
	var nodes = [];
	if(document.querySelectorAll){
		nodes = document.querySelectorAll('[data-stat]');
	} else {
		var allElements = document.all ? document.all : document.getElementsByTagName('*');
		for(var i = 0; i < allElements.length; i++){
			var node = allElements[i];
			if(node.dataset && node.dataset.stat){
				nodes.push(node);
			}
		}
		allElements = null;
	}

	for(var i = 0; i < nodes.length; i++){
		(function(node){
			node.addEventListener('click', function(){
				var data = node.dataset;
				var params = {};
				for(var key in data){
					var newKey = key.replace(/^stat(.)/, function(str, $1){
						return $1.toLowerCase();
					});
					data.hasOwnProperty(key) && /^stat.+$/.test(key) && (params[newKey] = data[key]);
				}
				Stat.sendEvent(data.stat, params);
			}, false);
		})(nodes[i]);
	}
}();

export default Stat;