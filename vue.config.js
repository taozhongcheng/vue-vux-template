module.exports = {
	baseUrl: './',
	//  process.env.NODE_ENV === 'production'
    // ? '/card-store-h5/'
    // : '/',
	outputDir: 'dist',
    assetsDir: 'static',
	devServer: {
		proxy: 'https://devagent.aijiatui.com/api/'
	},
	// vux 相关配置,使用vux-ui，和配置主题色
	configureWebpack: config => {
		require('vux-loader').merge(config, {
			options: {},
			plugins: [
				{ name: 'vux-ui' },
				{ name: 'less-theme', path: 'src/assets/less/theme.less' }
			]
		})
	}
}