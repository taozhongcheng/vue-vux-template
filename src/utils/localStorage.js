//localStorage管理文档文件,不要乱用，请备注说明每个存储对象
export const cacheKeys = {
    token: '网站token',
    userInfo: '用户信息'
}

export const setCache = function (key, value) {
    window.localStorage.setItem(key, value);
}

export const getCache = function (key) {
    return window.localStorage.getItem(key);
}

export const clearCache = function (key) {
    window.localStorage.removeItem(key);
}
