//请求处理文件
import axios from 'axios';
import router from '@/router'
import store from '@/store'
import Util from '@/utils/util'
import qs from 'qs';
//请求前处理
axios.interceptors.request.use(config => {
	//config.headers['Content-Type'] = 'application/json';
	const token = localStorage.getItem('token');
	if (token) {
		config.headers['Authorization'] = token;
    }
    config.data = qs.stringify(config.data);

	return config
}, error => {
	return Promise.reject(error);
});
// 请求后处理
axios.interceptors.response.use((response) => {

	if(response.data.code === 0){
		return response.data;
	}

	if(response.data.code === -2 || response.data.code === 1002) {
		Util.toast(response.data.msg || '会话已过期！');

		router.replace({
			name: 'login'
		});

		store.dispatch('clearLoginInfo');
		return;
	}

	if(!response.config.silentError){
		Util.toast(response.data.msg || response.data.message || '请求失败！');
	}

	return  -1 //异常状态码 Promise.reject(response.data);
}, error => {
	if(!error.config.silentError){
		Util.toast(`请求失败(${error.message})！`);
	}
		return -1 //异常状态码 Promise.reject(error);
});

export default axios;
