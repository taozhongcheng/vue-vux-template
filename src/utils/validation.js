//矫正文件归类
export default{
    //手机号验证
    phoneTest(phone) {
        const reg = /^1[3456789]\d{9}$/;
        return reg.test(phone)
    },
    //判断是否为空，空格，null，undefined，(0为真)
    isEmpty(str) {
        return str != null && str != undefined && str.toString().split(" ").join("").length !== 0
    },
    //是否为非0正整数
    intNumber(num){
        const reg = /^(\+?[1-9][0-9]*)$/;
        return reg.test(num)
    },
    //正确连接矫正
    urlTest(str){
         const reg =/^((https|http|ftp|rtsp|mms){0,1}(:\/\/){0,1})www\.(([A-Za-z0-9-~]+)\.)+([A-Za-z0-9-~\/])+$/;
         return reg.test(str)
    },
    //https开头网址矫正
    httpsTest(str){
       const reg =/^https/;
       return reg.test(str)
    },
    //身份证号码矫正
    codeTest(str){
        const reg = /(^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$)|(^[1-9]\d{5}\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}$)/;
        return reg.test(str)
    }
}