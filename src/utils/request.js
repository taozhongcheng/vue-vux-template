import axios from 'axios';

const axiometer = (f) => {
    return (url, params = {}, options = {}) => {
        const { withoutToken, ...restOptions } = options;
        // if (!options.withoutToken) {
        //     const token = localStorage.getItem('accessToken');
        //     params.token = token;
        // }
        return f(url, params, restOptions);
    };
};

export const get = axiometer((url, params, options) => {
    return axios.get(url, {
        params,
        ...options
    });
});

export const post = axiometer((url, params, options) => {
    return axios.post(url, params, {...options});
});

export const del = axiometer((url, params, options) => {
    return axios.delete(url, {
        params,
        ...options
    });
});
