import  Vue from 'vue';
export default {
	/** 路由跳转
	*  @param {object} ctx this对象
	*  @param {string} path 路由地址
	*  @param {object} query 传递参数
	*/
	toUrl(ctx, path, query) {
		ctx.$router.push({path, query})
	},
	/** 去除字符串的左右空格
	*  @param {string} value 字符串
	*/
	trim(value){
		if (typeof(value) === 'string')
			return value.replace(/(^\s*)|(\s*$)/g, '');
		return value
	},
	/** 提示弹窗
	*  @param {string} text 提示文字
	*  @param {string} type text,success, warn, cancel, text
	*  @param {string} time 显示时间
	*  @param {Boolen}} mask  是否要遮罩层
	*/
	toast(text, type = 'text', time = 2000, mask = false){
		Vue.$vux.toast.show({
			text,
			time,
			type,
			'is-show-mask':mask
		});
	},
	/**
	 * 确定弹窗
	 * @param {string} content 提示内容
	 * @param {function} onCancel 取消函数
	 * @param {function} onConfirm 确定函数
	 * @param {string} confirm 确定文字
	 * @param {string} cancel 取消文字
	 */
	confim(content,onConfirm, onCancel, confirm='确定', cancel='取消'){
		Vue.$vux.confirm.show({
			content,
			onCancel,
			onConfirm,
			'confirm-text': confirm,
			'cancel-text': cancel,
		})
	},
	/**
	 * loading加载框
	 * @param {string} text 提示文字
	 */
	loading(text='加载中...'){
		Vue.$vux.loading.show({
			text,
		})
	},
	/**
	 * 隐藏加载框
	 */
	hideLoading(){
		Vue.$vux.loading.hide()
	}
}
