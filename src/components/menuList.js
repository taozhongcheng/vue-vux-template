export const menuList = {
    data: [{
        "id": 10,
        "parent_id": 0,
        "display_name": "用户管理",
        "child": [
            {
                "id": 101,
                "parent_id": 10,
                "display_name": "用户列表",
                "url": "/admin/index",
            }
        ],
    }, {
        "id": 20,
        "parent_id": 0,
        "display_name": "角色管理",
        "child": [
            {
                "id": 201,
                "parent_id": 20,
                "display_name": "角色列表",
                "url": "/role/index",
            }
        ],
    },{
        "id": 40,
        "parent_id": 40,
        "display_name": "产品管理",
        "child": [
            {
                "id": 401,
                "parent_id": 40,
                "display_name": "产品列表",
                "url": "/product/index",
            }
        ]
    }, {
        "id": 50,
        "parent_id": 0,
        "display_name": "销售日志",
        "child": [
            {
                "id": 501,
                "parent_id": 50,
                "display_name": "销售列表",
                "url": "/sales/index",
            }
        ]
    }]
}



