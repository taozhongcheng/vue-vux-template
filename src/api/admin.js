import { apiEndpoint } from '@/config/config';
const admin ={
    // 登陆
    loginUrl: `${apiEndpoint}login`,
    editPasswordUrl: `${apiEndpoint}/user/updateUserInfo`,
    userListUrl: `${apiEndpoint}user/list`,
    userAddUrl: `${apiEndpoint}user/add`,
    userDeleteUrl: `${apiEndpoint}user/delete`,
    userDetailUrl: `${apiEndpoint}user/get`,
    userUpdateUrl: `${apiEndpoint}user/update`,
    // 角色
    roleListUrl: `${apiEndpoint}role/listAll`,
    // roleAddUrl: `${apiEndpoint}role/add`,
    // roleDetailUrl: `${apiEndpoint}role/get`,
    // roleUpdateUrl: `${apiEndpoint}role/update`,
    // roleDeleteUrl: `${apiEndpoint}role/delete`,
}
export default admin