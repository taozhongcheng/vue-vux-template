import { apiEndpoint } from '@/config/config';
const product = {
    // 产品
    productListUrl: `${apiEndpoint}hrProduct/list`,
    producAddUrl: `${apiEndpoint}hrProduct/add`,
    producDetailUrl: `${apiEndpoint}hrProduct/get`,
    producUpdateUrl: `${apiEndpoint}hrProduct/update`,
    producDeleteUrl: `${apiEndpoint}hrProduct/delete`,
    fileUploadUrl: `${apiEndpoint}file/upload/`,

    // 客户
    customerListUrl: `${apiEndpoint}hrCustomer/list`,
    customerAddUrl: `${apiEndpoint}hrCustomer/add`,
    customerDetailUrl: `${apiEndpoint}hrCustomer/get`,
    customerUpdateUrl: `${apiEndpoint}hrCustomer/update`,
    customerDeleteUrl: `${apiEndpoint}hrCustomer/delete`,
    // 销售
    orderListUrl: `${apiEndpoint}hrOrder/list`,
    orderAddUrl: `${apiEndpoint}hrOrder/add`,
    orderDetailUrl: `${apiEndpoint}hrOrder/get`,
    orderUpdateUrl: `${apiEndpoint}hrOrder/update`,
    orderDeleteUrl: `${apiEndpoint}hrOrder/delete`,
}
export default product