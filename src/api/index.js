//接口api统一输出文件
import  Admin from './admin';
import Product from './product';
const apiUrl = {
    ...Admin,
    ...Product
}

export default apiUrl;
