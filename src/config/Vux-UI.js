import Vue from 'vue';
// vux组件库，按需加载在这里
import {
	XInput,
	Group,
	//Confirm,
	ConfirmPlugin,
	LoadMore,
	Loading,
	LoadingPlugin,
	Msg,
	Alert,
	PopupHeader,
	//Toast,
	ToastPlugin,
	XDialog,
	Tab,
	TabItem,
	Tabbar,
	TabbarItem,
	XHeader,
	Badge,
	Card,
	Previewer,
	Swiper,
	XTable,
	CellBox,
	CellFormPreview,
	Cell,
	CheckIcon,
	Checklist,
	Datetime,
	InlineXNumber,
	PopupPicker,
	PopupRadio,
	Radio,
	Rater,
	Search,
	Selector,
	XAddress,
	XNumber,
	XSwitch,
	XTextarea,
	Flexbox,
	FlexboxItem,
	Grid,
	GridItem,
	Icon,
	XButton,

} from 'vux';
 
Vue.component('x-input', XInput)
Vue.component('group', Group)
// Vue.component('confirm', Confirm)
Vue.use(ConfirmPlugin)
Vue.component('load-more', LoadMore)
Vue.component('loading', Loading)
Vue.use(LoadingPlugin)
Vue.component('msg', Msg)
Vue.component('alert', Alert)
Vue.component('popup-header', PopupHeader)
// Vue.use(Toast)
Vue.use(ToastPlugin)
Vue.component('x-dialog', XDialog)
Vue.component('tab', Tab)
Vue.component('tab-item', TabItem)
Vue.component('tabbar', Tabbar)
Vue.component('tabbar-item', TabbarItem)
Vue.component('x-header', XHeader)
Vue.component('badge', Badge)
Vue.component('card', Card)
Vue.component('previewer', Previewer)
Vue.component('swiper', Swiper)
Vue.component('x-table', XTable)
Vue.component('cell-box', CellBox)
Vue.component('cell-form-preview', CellFormPreview)
Vue.component('cell', Cell)
Vue.component('check-icon', CheckIcon)
Vue.component('checklist', Checklist)
Vue.component('datetime', Datetime)
Vue.component('inline-x-number', InlineXNumber)
Vue.component('popup-picker', PopupPicker)
Vue.component('popup-radio', PopupRadio)
Vue.component('radio', Radio)
Vue.component('rater', Rater)
Vue.component('search', Search)
Vue.component('selector', Selector)
Vue.component('x-address', XAddress)
Vue.component('x-number', XNumber)
Vue.component('x-switch', XSwitch)
Vue.component('x-textarea', XTextarea)
Vue.component('flexbox', Flexbox)
Vue.component('flexbox-item', FlexboxItem)
Vue.component('grid', Grid)
Vue.component('grid-item', GridItem)
Vue.component('icon', Icon)
Vue.component('x-button', XButton)

// Vue.prototype.$toast  = ToastPlugin;