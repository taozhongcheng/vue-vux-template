 let apiEndpoint = null;

let host = window.location.host;

// 测试环境
if (host === `test-agent.aijiatui.com`){
   apiEndpoint = `https://test-agent.aijiatui.com/`;

// 开发环境
}else if (host === `dev-agent.aijiatui.com`){
   apiEndpoint = `https://dev-agent.aijiatui.com/`;

}else{
     apiEndpoint = `http://47.111.184.136/`;
}

export { apiEndpoint};

export const PROJECT_NAME = '加推';

// 上传腾讯云配置项
export const uploadProjectPath = 'release'; // 上传腾讯云根路径，以项目名称命名
export const Bucket = 'resource-1255821078';
export const Region = 'ap-guangzhou';
export const uploadEndpoint = `https://download.aijiatui.com/`;
export const fileEndpoint = `https://resource.aijiatui.com/`;
