// 路由统一输出文件
import Vue from 'vue';
import Router from 'vue-router';
import Admin from './admin';
Vue.use(Router);

const routes = [{
	name: 'home',
	path: '/home',
	meta: {
		title: '杭荣电气管理系统',
		needAuth: true
	},
	component: resolve => require(['../view/home/index'], resolve),
	children: [
		...Admin,
]
}, {
	name: 'login',
	path: '/login',
	component: resolve => require(['../view/login/login'], resolve),
	meta: {
		title: '用户登录'
	}
}, {
        path: '*', redirect: '/login'
}]

const router = new Router({
	routes
});


export default router;
