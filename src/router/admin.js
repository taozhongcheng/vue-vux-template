const admin =[
    {
        name: 'admin',
        path: '/admin/index',
        component: resolve => require(['../view/admin/index'], resolve),
        meta: {
            title: '用户管理',
            keepAlive: false,
            needAuth: true
        }
    }
]
export default admin;