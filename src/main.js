import Vue from 'vue'
import App from './App'
import router from '@/router'

import store from '@/store'

import '@/config/Vux-UI';

import '@/assets/less/base.less';
import '@/assets/less/common.less';
import '@/assets/less/style.less';

// import 'element-ui/lib/theme-chalk/index.css';

//import imageUpload from './components/ImageUpload.vue'
//Vue.component('image-upload', imageUpload);

import VueLazyload from 'vue-lazyload'

Vue.use(VueLazyload, {
	error: require('@/assets/image/icon_empty.png'),
	loading: require('@/assets/image/icon_empty.png'),
});

// import { polyfillUrl } from '@/utils/request';
// Vue.prototype.polyfillUrl = polyfillUrl;

import '@/utils/axios';

Vue.config.productionTip = false;
 

new Vue({
	router,
	store,
	render: h => h(App)
}).$mount('#app')

