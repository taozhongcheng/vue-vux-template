const tokenKey = 'token';
const userInfoKey = 'userInfo';

let userInfo = JSON.parse(localStorage.getItem(userInfoKey));
let token = userInfo ? userInfo.accessToken:'';
let role = localStorage.getItem('role');

const state = {
    TOKEN: token,
    USERINFO: userInfo || {},
    role: role || ''
}

const getters = {
    token: state => state.TOKEN,
    userInfo: state => state.USERINFO,
    role: state => state.role,
}

const mutations = {
    setUserInfo(state, data) {
        if (data) {
            localStorage.setItem(userInfoKey, JSON.stringify(data));
            localStorage.setItem(tokenKey, data.accessToken);
            state.TOKEN = data.accessToken;
        } else {
            localStorage.removeItem(userInfoKey);
            localStorage.removeItem(tokenKey);
            state.USERINFO = null;
        }
        state.USERINFO = data;
    },
    setRole(state, data) {
        if (data) {
            localStorage.setItem('role', data);
        } else {
            localStorage.removeItem('role');
        }
        state.role = data;
    }
}

const actions = {
    clearLoginInfo(state) {
        localStorage.removeItem(userInfoKey);
        localStorage.removeItem(tokenKey);
        localStorage.removeItem('role');
        state.commit('setUserInfo', null);
        state.commit('setRole', null);
    }
}

export default {
    state,
    getters,
    mutations,
    actions
}
