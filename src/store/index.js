//store统一输出文件
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import user from './module/user';

const store = new Vuex.Store({
	modules: {
		user		
	},
	strict: true
})

export default store;